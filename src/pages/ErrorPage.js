import React from 'react';
import { Link } from 'react-router-dom';
import {Button, Row, Col} from 'react-bootstrap'
export default function Banner(){
	return(
		<Row>
			<Col className="p-4">
				<h3>Zuitt Booking</h3>
				<h1>Page Not Found</h1>
				<p>Go back to <a href="/">homepage</a></p>
			</Col>
		</Row>
		)
}



