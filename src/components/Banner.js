//Destructuring components/modules for cleaner codebase
import {Button, Row, Col} from 'react-bootstrap'
export default function Banner(){
	return(
		<Row>
			<Col className="p-4 text-center">
				<h1>Zuitt Coding Bootcamp</h1>
				<p>Opprotunities for everyone, everywhere</p>
				<Button variant="primary">Enroll Now!</Button>
			</Col>
		</Row>
		)
}