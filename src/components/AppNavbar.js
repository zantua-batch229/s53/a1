import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import {Link, NavLink} from 'react-router-dom';

export default function AppNavbar(){
	return(
		<Navbar bg="light" expand="lg" className="p-3">
			<Navbar.Brand as={NavLink} exact to="/">Zuitt</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ml-auto">
					<Nav.Link as={NavLink} exact to="/">Home</Nav.Link>
					<Nav.Link as={NavLink} exact to="/courses">Courses</Nav.Link>
					<Nav.Link as={NavLink} exact to="/register">Register</Nav.Link>
					<Nav.Link as={NavLink} exact to="/login">Login</Nav.Link>
				</Nav>
			</Navbar.Collapse>
		</Navbar>

		)
}